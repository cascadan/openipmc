/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file front_leds.h
 * 
 * @author Andre Muller Cascadan
 * 
 * @brief  This header file contains the resources to manage the Front Panel LEDs.
 *
 */ 

#ifndef FRONT_LEDS_H
#define FRONT_LEDS_H


/**
 * @name Blue LED modes
 * @{
 */
#define BLUE_LED_OFF          0
#define BLUE_LED_SHORT_BLINK  1
#define BLUE_LED_LONG_BLINK   2
#define BLUE_LED_ON           3
///@}



/**
 * @brief Set the Blue LED mode
 * 
 * @param blue_led_mode Selected mode
 */
void front_leds_set_blue_led_mode(int blue_led_mode);






#endif // FRONT_LEDS_H
