
/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file fru_inventory_manager.c
 * 
 * @authors Bruno Augusto Casu
 * @authors Andre Muller Cascadan
 * 
 * @brief  Management interface for the FRU Inventory creation and access.
 */

#include "FreeRTOS.h"

#include "string.h"
#include "stdint.h"

#include "fru_inventory_manager.h"

static uint8_t  *fru_inventory;
static uint16_t fru_inventory_size = 0;

/*
 * ALLOCATION AND CREATION OF BOARD INFO ARRAY
 */
fru_inventory_field_t create_board_info_field (uint32_t    mfg_date_time,
                                               char* const board_manufacturer,
                                               char* const board_product_name,
                                               char* const board_serial_number,
                                               char* const board_part_number,
                                               char* const fru_file_id)
{
    size_t board_manufacturer_len = strlen( board_manufacturer);
    size_t board_product_name_len = strlen(board_product_name);
    size_t board_serial_number_len = strlen(board_serial_number);
    size_t board_part_number_len = strlen(board_part_number);
    size_t fru_file_id_len = strlen(fru_file_id);

    // Exception: ASCII fields can't have 1 byte. Only 0, 2 or more
    if( board_manufacturer_len  == 1 ) board_manufacturer_len  ++;
    if( board_product_name_len  == 1 ) board_product_name_len  ++;
    if( board_serial_number_len == 1 ) board_serial_number_len ++;
    if( board_part_number_len   == 1 ) board_part_number_len   ++;
    if( fru_file_id_len         == 1 ) fru_file_id_len         ++;

    // Protection against string too large
    if( board_manufacturer_len  > 63 ) board_manufacturer_len  = 63;
    if( board_product_name_len  > 63 ) board_product_name_len  = 63;
    if( board_serial_number_len > 63 ) board_serial_number_len = 63;
    if( board_part_number_len   > 63 ) board_part_number_len   = 63;
    if( fru_file_id_len         > 63 ) fru_file_id_len         = 63;

    // Calulate the size of this FRU Info Area
    int area_size = 13 + board_manufacturer_len + board_product_name_len + board_serial_number_len + board_part_number_len + fru_file_id_len;
                 // 13 = 6 bytes for header and manufacturing date + 5 bytes for each string type/length + 2 for C1h and Checksum

    // Adjust area size for multiple of 8
    int remain = area_size%8;
    area_size += (8-remain); // Add what is missing to reach the next multiple :)

    // Allocate space for the FRU Info Area
    uint8_t* board_info = (uint8_t*)pvPortMalloc( sizeof(uint8_t)*area_size );
    if( board_info == NULL )
        return NULL;

    // Area Header
    board_info[0] = 0x01;         // Board Area Format Version (01h standard specification)
    board_info[1] = area_size/8;  // Area size in chunks of 8 bytes
    board_info[2] = 25;           // Language set to: English

    // Manufacturing date/time
    board_info[3] = (uint8_t) ((mfg_date_time & 0x000000FF) >> 0 ); // DATE/TIME - Number of Minutes from 0:00 hrs 1/1/1996: 00 00 00h = unspecified ( LSbyte first, Little endian)
    board_info[4] = (uint8_t) ((mfg_date_time & 0x0000FF00) >> 8 );
    board_info[5] = (uint8_t) ((mfg_date_time & 0x00FF0000) >> 16); // MSByte

    int i;
    int offset = 6;

    // Board Manufacturer
    board_info[offset++] =  ASCII_CODE | board_manufacturer_len;
    for( i=0; i < board_manufacturer_len; i++ )
    {
       board_info[offset++] = board_manufacturer[i];
    }

    // Board Product Name
    board_info[offset++] = ASCII_CODE | board_product_name_len;
    for( i=0; i < board_product_name_len; i++ )
    {
       board_info[offset++] = board_product_name[i]; // Insert second string in Board Info Field
    }

    // Board Serial Number
    board_info[offset++] = ASCII_CODE | board_serial_number_len;
    for( i=0; i < board_serial_number_len; i++ )
    {
       board_info[offset++] = board_serial_number[i]; // Insert third string in Board Info Field
    }

    // Board Part Number
    board_info[offset++] = ASCII_CODE | board_part_number_len;
    for( i=0; i < board_part_number_len; i++ )
    {
       board_info[offset++] = board_part_number[i]; // Insert fourth string in Board Info Field
    }

    // FRU File ID
    board_info[offset++] = ASCII_CODE | fru_file_id_len;
    for( i=0; i < fru_file_id_len; i++ )
    {
       board_info[offset++] = fru_file_id[i]; // Insert last string in Board Info Field
    }

    // End of info fields
    board_info[offset++] = 0xc1;

    // Complete the rest of space with Zeroes
    while( offset < area_size )
    {
    	 board_info[offset++] = 0x00;
    }

    // Calculate the checksum
    uint8_t board_area_sum = 0;
    for( i=0; i < area_size; i++ )
    {
        board_area_sum += board_info[i];
    }
    board_info[area_size-1] = (~board_area_sum)+1; // Add checksum into the last byte

    return board_info;
}



/*
 * ALLOCATION AND CREATION OF PRODUCT INFO ARRAY
 */
fru_inventory_field_t create_product_info_field (char * const manufacturer_name,
                                               char * const product_name,
                                               char * const product_part_model_number,
                                               char * const product_version,
                                               char * const product_serial_number,
                                               char * const asset_tag,
                                               char * const fru_file_id)
{
    size_t manufacturer_name_len          = strlen( manufacturer_name         );
    size_t product_name_len               = strlen( product_name              );
    size_t product_part_model_number_len  = strlen( product_part_model_number );
    size_t product_version_len            = strlen( product_version           );
    size_t product_serial_number_len      = strlen( product_serial_number     );
    size_t asset_tag_len                  = strlen( asset_tag                 );
    size_t fru_file_id_len                = strlen( fru_file_id               );

    // Exception: ASCII fields can't have 1 byte. Only 0, 2 or more
    if( manufacturer_name_len         == 1 ) manufacturer_name_len         ++;
    if( product_name_len              == 1 ) product_name_len              ++;
    if( product_part_model_number_len == 1 ) product_part_model_number_len ++;
    if( product_version_len           == 1 ) product_version_len           ++;
    if( product_serial_number_len     == 1 ) product_serial_number_len     ++;
    if( asset_tag_len                 == 1 ) asset_tag_len                 ++;
    if( fru_file_id_len               == 1 ) fru_file_id_len               ++;

    // Protection against string too large
    if( manufacturer_name_len         > 63 ) manufacturer_name_len         = 63;
    if( product_name_len              > 63 ) product_name_len              = 63;
    if( product_part_model_number_len > 63 ) product_part_model_number_len = 63;
    if( product_version_len           > 63 ) product_version_len           = 63;
    if( product_serial_number_len     > 63 ) product_serial_number_len     = 63;
    if( asset_tag_len                 > 63 ) asset_tag_len                 = 63;
    if( fru_file_id_len               > 63 ) fru_file_id_len               = 63;

    // Calulate the size of this FRU Info Area
    int area_size = 12 + manufacturer_name_len + product_name_len + product_part_model_number_len + product_version_len + product_serial_number_len + asset_tag_len + fru_file_id_len;
                 // 12 = 3 bytes for header and manufacturing date + 7 bytes for each string type/length + 2 for C1h and Checksum

    // Adjust area size for multiple of 8
    int remain = area_size%8;
    area_size += (8-remain); // Add what is missing to reach the next multiple :)

    // Allocate space for the FRU Info Area
    uint8_t* board_info = (uint8_t*)pvPortMalloc( sizeof(uint8_t)*area_size );
    if( board_info == NULL )
        return NULL;

    // Area Header
    board_info[0] = 0x01;         // Board Area Format Version (01h standard specification)
    board_info[1] = area_size/8;  // Area size in chunks of 8 bytes
    board_info[2] = 25;           // Language set to: English

    int i;
    int offset = 3;

    // Manufacturer Name
    board_info[offset++] =  ASCII_CODE | manufacturer_name_len;
    for( i=0; i < manufacturer_name_len; i++ )
    {
        board_info[offset++] = manufacturer_name[i];
    }

    // Product name
    board_info[offset++] =  ASCII_CODE | product_name_len;
    for( i=0; i < product_name_len; i++ )
    {
        board_info[offset++] = product_name[i];
    }

    // Product Part/Model Number
    board_info[offset++] =  ASCII_CODE | product_part_model_number_len;
    for( i=0; i < product_part_model_number_len; i++ )
    {
        board_info[offset++] = product_part_model_number[i];
    }

    // Product Version
    board_info[offset++] =  ASCII_CODE | product_version_len;
    for( i=0; i < product_version_len; i++ )
    {
        board_info[offset++] = product_version[i];
    }

    // Product Serial Number
    board_info[offset++] =  ASCII_CODE | product_serial_number_len;
    for( i=0; i < product_serial_number_len; i++ )
    {
        board_info[offset++] = product_serial_number[i];
    }

    // Asset Tag
    board_info[offset++] =  ASCII_CODE | asset_tag_len;
    for( i=0; i < asset_tag_len; i++ )
    {
        board_info[offset++] = asset_tag[i];
    }

    // FRU File ID
    board_info[offset++] =  ASCII_CODE | fru_file_id_len;
    for( i=0; i < fru_file_id_len; i++ )
    {
        board_info[offset++] =fru_file_id[i];
    }

    // End of info fields
    board_info[offset++] = 0xc1;

    // Complete the rest of space with Zeroes
    while( offset < area_size )
    {
    	 board_info[offset++] = 0x00;
    }

    // Calculate the checksum
    uint8_t board_area_sum = 0;
    for( i=0; i < area_size; i++ )
    {
        board_area_sum += board_info[i];
    }
    board_info[area_size-1] = (~board_area_sum)+1; // Add checksum into the last byte

    return board_info;
}



/*
 * ASSEMBLE FRU INVENTORY USING CREATED FIELDS
 */
void create_fru_inventory ( fru_inventory_field_t internal_use,
                            fru_inventory_field_t chassis_info,
                            fru_inventory_field_t board_info,
                            fru_inventory_field_t product_info,
                            fru_inventory_field_t multi_record,
                                         uint16_t multi_record_size )
{
    size_t aux_size[] = {0,0,0,0,0};
    if (internal_use != NULL) {aux_size[0] = 8*internal_use[1];}
    if (chassis_info != NULL) {aux_size[1] = 8*chassis_info[1];}
    if (board_info   != NULL) {aux_size[2] = 8*board_info  [1];}
    if (product_info != NULL) {aux_size[3] = 8*product_info[1];}
    if (multi_record != NULL) {aux_size[4] = multi_record_size;}

    // Calculate the size of entire inventory
    fru_inventory_size = 8 + aux_size[0] + aux_size[1] + aux_size[2] + aux_size[3] + aux_size[4];

    fru_inventory = pvPortMalloc( sizeof(uint8_t)*fru_inventory_size );
    if( fru_inventory == NULL )
        return;

    // Common Header Format Version - 01h for the specification used
    fru_inventory[0] = 0x01; 

    int header_offset = 1; // Offset of the first area

    //auxiliary var for copying data. In bytes.
    int i;
    int real_offset; 
    int real_size;
    
    if(internal_use == NULL)
        fru_inventory[1] = 0;  // Offset zero if area is absent
    else 
    { 
        fru_inventory[1] = header_offset; // Add the area offset into the header
        real_offset = 8*header_offset;
        real_size = 8*internal_use[1];
        for( i=0; i<real_size; i++)
            fru_inventory[real_offset+i] = internal_use[i]; // Copy area
        header_offset += internal_use[1];  // Calculate offset for next area
    }

    if(chassis_info == NULL)
        fru_inventory[2] = 0;
    else
    {
        fru_inventory[2] = header_offset;
        real_offset = 8*header_offset;
        real_size = 8*chassis_info[1];
        for( i=0; i<real_size; i++)
            fru_inventory[real_offset+i] = chassis_info[i];
        header_offset += chassis_info[1];
    }

    if(board_info == NULL)
        fru_inventory[3] = 0;
    else
    {
        fru_inventory[3] = header_offset;
        real_offset = 8*header_offset;
        real_size = 8*board_info[1];
        for( i=0; i<real_size; i++)
            fru_inventory[real_offset+i] = board_info[i];
        header_offset += board_info[1];
    }

    if(product_info == NULL)
        fru_inventory[4] = 0;
    else
    {
        fru_inventory[4] = header_offset;
        real_offset = 8*header_offset;
        real_size = 8*product_info[1];
        for( i=0; i<real_size; i++)
            fru_inventory[real_offset+i] = product_info[i];
        header_offset += product_info[1];
    }

    if(multi_record == NULL)
        fru_inventory[5] = 0;
    else
    {
        fru_inventory[5] = header_offset;
        real_offset = 8*header_offset;
        real_size = multi_record_size;
        for( i=0; i<real_size; i++)
            fru_inventory[real_offset+i] = multi_record[i];
    }

    fru_inventory[6] = 0x00; // Reserved. Write as 00h

    uint8_t header_sum = 0x00;
    for (i=0; i<7; i++)
    {
        header_sum += fru_inventory[i];
    }
    fru_inventory[7] = (~header_sum) +1; // Add Common Header checksum


}



/*
 * Get the size of FRU Inventory
 */
int get_fru_inventory_size( uint8_t fru_id )
{
    if (fru_id == 0x00)
        return fru_inventory_size;

    else
    	return -1; //Invalid FRU Device ID
}



/*
 * Get a segment of the FRU Inventory
 */
int get_fru_inventory_data( uint8_t fru_id, uint16_t offset, uint8_t lenght , uint8_t returned_bytes[] )
{
	int n;

    if (fru_id == 0x00)
    {
        for (n = 0 ; n < lenght; n++)
        {
        	if ( offset + n >= fru_inventory_size )
        		break; // Stop if reaches the end

        	returned_bytes[n] = fru_inventory[offset + n];
        }

        return n;
    }
    else
    	return -1; //Invalid FRU Device ID
}





