/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file sensors_templates.c
 * 
 * @authors Bruno Augusto Casu
 * @authors André Muller Cascadan
 * 
 * @brief  Template functions to create SDR
 *
 */

#include "string.h"
#include "stdint.h"

#include "ipmb_0.h"
#include "ipmc_ios.h"
#include "sdr_definitions.h"
#include "sdr_manager.h"
#include "fru_state_machine.h"

/*
 * Allocate record and Load with standard info for the Hot Swap Carrier (SDR Type 01h)
 * Sensor number is configured to be the same as the allocated record id
 */
void create_hot_swap_carrier_sensor (char* id_string)
{
    uint16_t const record_id = allocate_sdr (FULL_SENSOR_RECORD);
    uint8_t  const sensor_number = (uint8_t) ((record_id & 0x00FF) >> 0);
    sdr_type_01_t* record = allrecords[record_id].ptr;
    allrecords[record_id].get_sensor_reading_cb = &fru_hot_swap_sensor_reading;
        
    //Header                                                                                            
    record->record_id =                                        record_id;                               
    record->sdr_version =                                      IPMI_VERSION_1_5;                        
    record->record_type_number =                               RECORD_TYPE_01H;                         
    //Key Bytes                                                                                         
    record->sensor_owner_id =                                  ipmb_0_addr;
    record->channel_number =                                   NOT_USED_FIELD;                          
    record->sensor_owner_lun =                                 LUN_O;                                   
    record->sensor_number =                                    sensor_number;                           
    //Body                                                                                              
    record->entity_id =                                        PICMG_FRONT_BOARD_ENTITY_ID;
    record->entity_instance =                                  PHYSICAL_ENTITY | DEVICE_RELATIVE_NUM_0;                                           
        //Initialization                                                                              
    record->initialization =                                   INIT_SCANNING_ENABLE | INIT_EVENTS_ENABLE | INIT_THRESHOLDS_ENABLE | INIT_SENSOR_TYPE_ENABLE | INIT_SENSOR_TYPE_ENABLE;                                    
    record->sensor_default_power_up_state =                    EVENT_GENERATION_ENABLED | SENSOR_SCANNING_ENABLED;                              
        //Capabilities                                                                                 
    record->sensor_status_check =                              DO_NOT_IGNORE_SENSOR;                   
    record->sensor_auto_rearm =                                AUTO_REARM_YES;                         
    record->sensor_hysteresis_support =                        NO_HYSTERESIS;                          
    record->sensor_threshold_access_support =                  NO_THRESHOLDS;                          
    record->sensor_event_msg_ctrl_support =                    ENTIRE_SENSOR_ONLY;                     
    record->sensor_type =                                      HOT_SWAP_CARRIER;
    record->event_reading_type =                               EVENT_READING_SPECIFIC;
        //Masks
    record->assertion_event_mask_for_non_threshold_sensor =    NOT_USED_FIELD;
    record->lower_threshold_reading_mask =                     NOT_USED_FIELD;
    record->threshold_assertion_event_mask =                   NOT_USED_FIELD;
    record->deassertion_event_mask_for_non_threshold_sensor =  NOT_USED_FIELD;
    record->upper_threshold_reading_mask =                     NOT_USED_FIELD;
    record->threshold_deassertion_event_mask =                 NOT_USED_FIELD;
    record->reading_mask_for_non_threshold_sensor =            NOT_USED_FIELD;
    record->settable_threshold_mask =                          NOT_USED_FIELD;
    record->readable_threshold_mask =                          NOT_USED_FIELD;
        //Data Format
    record->analog_data_format =                               NOT_USED_FIELD;
    record->rate_unit =                                        NOT_USED_FIELD;
    record->modifier_unit =                                    NOT_USED_FIELD;
    record->percentage =                                       NOT_USED_FIELD;
    record->sensor_base_unit =                                 NOT_USED_FIELD;
    record->sensor_modifier_unit =                             NOT_USED_FIELD;
    record->linearization =                                    NOT_USED_FIELD;
    record->m =                                                NOT_USED_FIELD;
    record->tolerance =                                        NOT_USED_FIELD;
    record->b =                                                NOT_USED_FIELD;
    record->accuracy =                                         NOT_USED_FIELD;
    record->accuracy_exp =                                     NOT_USED_FIELD;
    record->r_exp =                                            NOT_USED_FIELD;
    record->b_exp =                                            NOT_USED_FIELD;
        // Ranges
    record->analog_characteristics =                           NOT_USED_FIELD;
    record->nominal_reading =                                  0xd0;
    record->normal_maximum =                                   NOT_USED_FIELD;
    record->normal_minimum =                                   NOT_USED_FIELD;
    record->sensor_maximum_reading =                           0xff;
    record->sensor_minimum_reading =                           0x00;
        //Threshold Settings
    record->upper_non_recoverable_threshold =                  0xc0;
    record->upper_critical_threshold =                         NOT_USED_FIELD;
    record->upper_non_critical_threshold =                     NOT_USED_FIELD;
    record->lower_non_recoverable_threshold =                  NOT_USED_FIELD;
    record->lower_critical_threshold =                         NOT_USED_FIELD;
    record->lower_non_critical_threshold =                     NOT_USED_FIELD;
    record->positive_threshold_hysteresis_value =              NOT_USED_FIELD;
    record->negative_threshold_hysteresis_value =              NOT_USED_FIELD;
        //ID String Config
    record->id_string_type =                                   ASCII_FORMAT;
    record->id_string_length =                                 strlen(id_string);
    record->id_string =                                        id_string;
}


/*
 * Allocate record and Load with standard info for the IPMB-0 sensor (SDR Type 01h)
 * Sensor number is configured to be the same as the allocated record id
 */
void create_ipmb0_sensor (char* id_string)
{
    uint16_t const record_id = allocate_sdr (FULL_SENSOR_RECORD);
    uint8_t  const sensor_number = (uint8_t) ((record_id & 0x00FF) >> 0);
    sdr_type_01_t* record = allrecords[record_id].ptr;
    allrecords[record_id].get_sensor_reading_cb = &ipmb0_sensor_reading;

    //Header
    record->record_id =                                        record_id;
    record->sdr_version =                                      IPMI_VERSION_1_5;
    record->record_type_number =                               RECORD_TYPE_01H;
    //Key Bytes
    record->sensor_owner_id =                                  ipmb_0_addr;
    record->channel_number =                                   NOT_USED_FIELD;
    record->sensor_owner_lun =                                 LUN_O;
    record->sensor_number =                                    sensor_number;
    //Body
    record->entity_id =                                        PICMG_FRONT_BOARD_ENTITY_ID;
    record->entity_instance =                                  PHYSICAL_ENTITY | DEVICE_RELATIVE_NUM_0;
        //Initialization
    record->initialization =                                   INIT_SCANNING_ENABLE | INIT_EVENTS_ENABLE | INIT_THRESHOLDS_ENABLE | INIT_SENSOR_TYPE_ENABLE | INIT_SENSOR_TYPE_ENABLE;
    record->sensor_default_power_up_state =                    EVENT_GENERATION_ENABLED | SENSOR_SCANNING_ENABLED;
        //Capabilities
    record->sensor_status_check =                              DO_NOT_IGNORE_SENSOR;
    record->sensor_auto_rearm =                                AUTO_REARM_YES;
    record->sensor_hysteresis_support =                        NO_HYSTERESIS;
    record->sensor_threshold_access_support =                  NO_THRESHOLDS;
    record->sensor_event_msg_ctrl_support =                    ENTIRE_SENSOR_ONLY;
    record->sensor_type =                                      IPMB0_SENSOR;
    record->event_reading_type =                               EVENT_READING_SPECIFIC;                 
        //Masks                                                                                        
    record->assertion_event_mask_for_non_threshold_sensor =    NOT_USED_FIELD;                         
    record->lower_threshold_reading_mask =                     NOT_USED_FIELD;                         
    record->threshold_assertion_event_mask =                   NOT_USED_FIELD;                         
    record->deassertion_event_mask_for_non_threshold_sensor =  NOT_USED_FIELD;                         
    record->upper_threshold_reading_mask =                     NOT_USED_FIELD;                         
    record->threshold_deassertion_event_mask =                 NOT_USED_FIELD;                         
    record->reading_mask_for_non_threshold_sensor =            NOT_USED_FIELD;                         
    record->settable_threshold_mask =                          NOT_USED_FIELD;                         
    record->readable_threshold_mask =                          NOT_USED_FIELD;                         
        //Data Format                                                                                  
    record->analog_data_format =                               NOT_USED_FIELD;                         
    record->rate_unit =                                        NOT_USED_FIELD;                         
    record->modifier_unit =                                    NOT_USED_FIELD;                         
    record->percentage =                                       NOT_USED_FIELD;                         
    record->sensor_base_unit =                                 NOT_USED_FIELD;                         
    record->sensor_modifier_unit =                             NOT_USED_FIELD;                         
    record->linearization =                                    NOT_USED_FIELD;                         
    record->m =                                                NOT_USED_FIELD;                         
    record->tolerance =                                        NOT_USED_FIELD;                         
    record->b =                                                NOT_USED_FIELD;                         
    record->accuracy =                                         NOT_USED_FIELD;                         
    record->accuracy_exp =                                     NOT_USED_FIELD;                         
    record->r_exp =                                            NOT_USED_FIELD;                         
    record->b_exp =                                            NOT_USED_FIELD;                         
        // Ranges                                                                                      
    record->analog_characteristics =                           NOT_USED_FIELD;                     
    record->nominal_reading =                                  0xd0;                                   
    record->normal_maximum =                                   NOT_USED_FIELD;                         
    record->normal_minimum =                                   NOT_USED_FIELD;                         
    record->sensor_maximum_reading =                           0xff;                     
    record->sensor_minimum_reading =                           0x00;                     
        //Threshold Settings                                                                           
    record->upper_non_recoverable_threshold =                  0xc0;                                   
    record->upper_critical_threshold =                         NOT_USED_FIELD;                         
    record->upper_non_critical_threshold =                     NOT_USED_FIELD;                         
    record->lower_non_recoverable_threshold =                  NOT_USED_FIELD;                         
    record->lower_critical_threshold =                         NOT_USED_FIELD;                          
    record->lower_non_critical_threshold =                     NOT_USED_FIELD;                          
    record->positive_threshold_hysteresis_value =              NOT_USED_FIELD;                          
    record->negative_threshold_hysteresis_value =              NOT_USED_FIELD;                          
        //ID String Config                                                                              
    record->id_string_type =                                   ASCII_FORMAT;                            
    record->id_string_length =                                 strlen(id_string);                       
    record->id_string =                                        id_string;                               
}


/*
 * Allocate record and Load with info for a generic analog sensor sensor (SDR Type 01h)
 *
 * y = [ M*x + (B * 10^Be ) ] * 10^Re
 */
void create_generic_analog_sensor_1 (uint8_t sensor_type, uint8_t base_unit_type, int16_t m, int16_t b, int8_t b_exp, int8_t r_exp , uint8_t threshold_mask, uint8_t* threshold_list, char* id_string, sensor_reading_status_t (*get_sensor_reading_func)(sensor_reading_t*))
{
    uint16_t const record_id = allocate_sdr (FULL_SENSOR_RECORD);
    uint8_t  const sensor_number = (uint8_t) (record_id & 0x00FF);
    sdr_type_01_t* record = allrecords[record_id].ptr;
    allrecords[record_id].get_sensor_reading_cb = get_sensor_reading_func;

    uint8_t sensor_threshold_access_support = NO_THRESHOLDS;
    if ( threshold_mask != 0 )
    	sensor_threshold_access_support = READBLE_TH;

    //Header
    record->record_id =                                        record_id;
    record->sdr_version =                                      IPMI_VERSION_1_5;
    record->record_type_number =                               RECORD_TYPE_01H;
    //Key Bytes
    record->sensor_owner_id =                                  ipmb_0_addr;
    record->channel_number =                                   NOT_USED_FIELD;
    record->sensor_owner_lun =                                 LUN_O;
    record->sensor_number =                                    sensor_number;
    //Body
    record->entity_id =                                        PICMG_FRONT_BOARD_ENTITY_ID;
    record->entity_instance =                                  PHYSICAL_ENTITY | DEVICE_RELATIVE_NUM_0;
    //Initialization
    record->initialization =                                   INIT_SENSOR_TYPE_ENABLE;
    record->sensor_default_power_up_state =                    SENSOR_SCANNING_ENABLED;
    //Capabilities
    record->sensor_status_check =                              DO_NOT_IGNORE_SENSOR;
    record->sensor_auto_rearm =                                AUTO_REARM_YES;
    record->sensor_hysteresis_support =                        NO_HYSTERESIS;
    record->sensor_threshold_access_support =                  sensor_threshold_access_support;
    record->sensor_event_msg_ctrl_support =                    NO_EVENT_FROM_SENSOR;
    record->sensor_type =                                      sensor_type;
    record->event_reading_type =                               EVENT_READING_THRESHOLD;
    //Masks
    record->assertion_event_mask_for_non_threshold_sensor =    NOT_USED_FIELD;
    record->lower_threshold_reading_mask =                     threshold_mask & 0x07;
    record->threshold_assertion_event_mask =                   NOT_USED_FIELD;
    record->deassertion_event_mask_for_non_threshold_sensor =  NOT_USED_FIELD;
    record->upper_threshold_reading_mask =                     (threshold_mask >> 3) & 0x07;
    record->threshold_deassertion_event_mask =                 NOT_USED_FIELD;
    record->reading_mask_for_non_threshold_sensor =            NOT_USED_FIELD;
    record->settable_threshold_mask =                          NOT_USED_FIELD;
    record->readable_threshold_mask =                          threshold_mask;
    //Data Format
    record->analog_data_format =                               UNSIGNED_DATA;
    record->rate_unit =                                        NOT_USED_FIELD;
    record->modifier_unit =                                    NOT_USED_FIELD;
    record->percentage =                                       NOT_USED_FIELD;
    record->sensor_base_unit =                                 base_unit_type;
    record->sensor_modifier_unit =                             NOT_USED_FIELD;
    record->linearization =                                    LINEAR;
    record->m =                                                m;
    record->tolerance =                                        1;
    record->b =                                                b;
    record->accuracy =                                         0;
    record->accuracy_exp =                                     0;
    record->r_exp =                                            r_exp;
    record->b_exp =                                            b_exp;
        // Ranges
    record->analog_characteristics =                           NOT_USED_FIELD;
    record->nominal_reading =                                  NOT_USED_FIELD;
    record->normal_maximum =                                   NOT_USED_FIELD;
    record->normal_minimum =                                   NOT_USED_FIELD;
    record->sensor_maximum_reading =                           0xff;
    record->sensor_minimum_reading =                           0x00;
        //Threshold Settings
    record->upper_non_recoverable_threshold =                  threshold_list[5];
    record->upper_critical_threshold =                         threshold_list[4];
    record->upper_non_critical_threshold =                     threshold_list[3];
    record->lower_non_recoverable_threshold =                  threshold_list[0];
    record->lower_critical_threshold =                         threshold_list[1];
    record->lower_non_critical_threshold =                     threshold_list[2];
    record->positive_threshold_hysteresis_value =              NOT_USED_FIELD;
    record->negative_threshold_hysteresis_value =              NOT_USED_FIELD;
        //ID String Config
    record->id_string_type =                                   ASCII_FORMAT;
    record->id_string_length =                                 strlen(id_string) & 0xFF;
    if (record->id_string_length > 16)
        record->id_string_length = 16; // Max of 16 characters
    record->id_string =                                        id_string;
}

