/****************************************************************************/
/*                                                                          */
/* This Source Code Form is subject to the terms of the Mozilla Public      */
/* License, v. 2.0. If a copy of the MPL was not distributed with this      */
/* file, You can obtain one at http://mozilla.org/MPL/2.0/.                 */
/*                                                                          */
/****************************************************************************/

/*
 * This file is part of the OpenIPMC project.
 */

/**
 * @file ipmi_netfn_app.c
 * 
 * @authors Alison Franca Queiroz da Costa
 * @authors Andre Muller Cascadan
 * @authors Bruno Augusto Casu
 * 
 * @brief  Response functions for Application Network Function commands.
 *
 * This file contains the specific functions to manage the requests of Application Network Function commands (defined in PICMG v3.0 and IPMI v1.5).
 * Each function returns the response data bytes and the completion code.
 * 
 * The specific functions are called by ipmi_msg_solve_request_ipmb0().    
 */

#include "stdint.h"
#include "ipmc_ios.h"
#include "device_id.h"


/**
 * @{
 * @name Application NetFn commands
 */

/**
 * @brief Specific function to provide a response for "Get Device ID" command.
 * 
 * @param request_bytes   Data bytes in the IPMI request.
 * @param req_len         Number of data bytes in the IPMI request. The size of the request can be different depending on the command.                        
 * @param response_bytes  Data bytes returned by the this function (definition in IMPI v1.5). 
 * @param res_len         Number of bytes in the response returned.
 * @param completion_code Byte returned by the response function to identify the Completion Code (definition in IPMI v1.5).
 */
void ipmi_cmd_get_device_id( uint8_t request_bytes[], int req_len, uint8_t response_bytes[], int *res_len, uint8_t *completion_code )
{

    response_bytes[0] = 0x00;	// Device ID
    
    // Device Revision (Bit 7 set as 1: Device provides SDR)
    response_bytes[1] = (ipmc_device_id.device_revision & 0x0F) | 0x80;

    // Major Firmware Revision (Bit 7 set as 0: Device available: normal operation)
    response_bytes[2] = ipmc_device_id.firmware_major_revision & 0x7F;
    
    // Minor Firmware Revision (BCD)
    response_bytes[3] = ipmc_device_id.firmware_minor_revision;
    
    response_bytes[4] = 0x51;   // IPMI Version v1.5 (bits 7:4 holds the Least Significant digit, bits 3:0 holds the Most Significant digit)
    
    // Additional Device Support
    response_bytes[5] = ipmc_device_id.device_support;

    // Manufacturer ID
    response_bytes[6] = (ipmc_device_id.manufacturer_id    )&0xFF;
    response_bytes[7] = (ipmc_device_id.manufacturer_id>>8 )&0xFF;
    response_bytes[8] = (ipmc_device_id.manufacturer_id>>16)&0xFF;

    // Product ID
    response_bytes[9]  = (ipmc_device_id.product_id   )&0xFF;
    response_bytes[10] = (ipmc_device_id.product_id>>8)&0xFF;

    // Auxiliary Firmware Revision
    response_bytes[11] = ipmc_device_id.auxiliar_firmware_rev_info[0];
    response_bytes[12] = ipmc_device_id.auxiliar_firmware_rev_info[1];
    response_bytes[13] = ipmc_device_id.auxiliar_firmware_rev_info[2];
    response_bytes[14] = ipmc_device_id.auxiliar_firmware_rev_info[3];

    *res_len = 15;
    *completion_code = 0; //OK
}

///@}
